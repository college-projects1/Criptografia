"""
Decifra Mensagem cifrada com a cifra Rail Fance
Entrada: Arquivo com mensagem cifrada
Sainda: Arquivo com mensagem decifrada
"""

def main():
    f = open("texto.txt")
    msg = f.read()
    f.close()
    for k in range(2, len(msg)):
        txt = decode(k, msg)
        a = open("out"+str(k)+".txt", 'w')
        print(txt)
        a.write(txt)
        a.close()
        r = input("Deseja continuar?(S/n) ")
        if(r == "n" or r == "N"):
            return

def decode(k, msg):
    charsForCycle = (2*k)-2
    cycles = len(msg)//charsForCycle
    rest =  len(msg)%charsForCycle
    charsForRail = []
    charsInSemiCycle = [0 for i in range(k)]
    rails =[]
    text = ""
    
    # Preenche @param <charsForRail> com o numero de letras em cada trilhos
    charsForRail.append(cycles)
    for i in range(k-2):
        charsForRail.append(2*cycles)
    charsForRail.append(cycles)

    # Preenche @param <charsInSemiCycle> com o numero de letras em cada trilhos do semi-ciclo
    n = rest
    for j in range(k):
        if(n):
            charsInSemiCycle[j] = +1
            n-=1
    for x in range(j-1, 0, -1):
        if(n):
            charsInSemiCycle[j] = +1
            n-=1
    # Preenche @param <rails> com as substrings correspondentes a cada trilho
    n = 0
    for i in range(k):
        s = ""
        for j in range(charsForRail[i] + charsInSemiCycle[i]):
                s+=msg[j+n]
        rails.append(s)
        n += charsForRail[i] + charsInSemiCycle[i]

    #Alterna o ultimo elemento de @param <rails> entre letras e '#'
    s = ""
    for i in rails[0]:
        s+=i+"#"
    rails[0] = s
    
    #Alterna o ultimo elemento de @param <rails> entre letras e '*'
    s = ""
    for i in rails[-1]:
        s+=i+"*"
    rails[-1] = s
    """
    Preenche @param <text> com os caracteres de cada trillho de forma ordenada
    @param <charsInSemiCycle[0]>
                    somente pode assumir os valores 0 ou 1
    """
    n, strIndex = 0, 0
    while(n<cycles+charsInSemiCycle[0]):
        for i in range(k):
            if(len(rails[i])>strIndex):
                text += rails[i][strIndex]
        strIndex += 1
        for j in range(k-2, 0, -1):
            if(len(rails[i])>strIndex):
                text += rails[j][strIndex]
        strIndex += 1
        n+=1
    return text
main()
