from math import *

"""
Fatora um numero 'n' utilizando o algoritimo de Fermat
Retorna uma tupla com fatores de 'n'
"""
def fermat(n):
    x = int(sqrt(n))
    y = 0

    while(n != pow(x, 2)-pow(y, 2)):
        x += 1
        y = int(sqrt(pow(x, 2)-n))
		
        if(x == (x+1)/2):
            return(0, 0)

    return (x+y, x-y)
    
"""
Calcula o MDC entre 'a' e 'b', utilizando o Algoritimo Euclidiano Extendido
Retorna uma tupla com (mdc, alfa, beta)
"""
def aEExtendido(a, b):
    resto, x, y = 0, 0, 0
    alfa = [1, 0] # Valores anteriores de x 
    beta = [0, 1] # Valores anteriores de y
    while(a%b!=0):
        quociente = a//b
        resto = a%b
        a = b
        b = resto
        x = alfa[0]-(quociente*alfa[1])
        
        alfa[0] = alfa[1]
        alfa[1] = x
        
        y = beta[0]-(quociente*beta[1])
        beta[0] = beta[1]
        beta[1] = y
        
    return (resto, x, y)

"""
Calcula potencia 'k' base 'a' modulo 'n'
"""
def powMOD(a, k, n):
	R = 1
	A = a
	E = k
	
	while(E!=0):
		if(E%2):
			R *= A
			E = (E-1)/2
		else:
			E /= 2
		A = (A*A)%n
	return R%n
	
"""
Retorna fi de 'n'
"""
def fi(n):
	return n-1

"""
Converte e escreve msg decifrada
"""
def escreve(char):
	f = open("out.txt", "a+")
	char = int(char)
	if(char == 99):
		char = 32
	else:
		char+=55
	f.write(chr(char))
	f.close()
	
"""
Quebra cifra RSA
"""
def main():
	msg = ""
	blocos = []
	n = 100100420190437
	e = 99410052592945
	
	print("n: {} e: {}".format(n, e))
	
	f = open("texto.txt")
	if(f == None):
		return -1
	
	blocos = f.readlines()
	for i in range(len(blocos)):
		blocos[i] = int(blocos[i].strip())
	
	print("msg: {}".format(blocos))
	
	print("Fatorando "+str(n))
	fator = fermat(n)
	print(str(n)+" fatoradao", fator)
	
	fiN = fi(fator[0])*fi(fator[1])
	print("fi de N calculado")
	
	mdc = aEExtendido(e, fiN)
	
	if(mdc[0] != 1):
		return -1
	print("MDC entre 'e' e fi de N calculado!")
	
	d = mdc[1]
	
	print("\nCalcular potencias")
	for b in blocos:
		msg+=str((powMOD(b, d, n)))
		print("potencia {} base {} mod {} calculada!".format(d, b, n))
	f.close()
	
	for i in range(0, len(msg), 2):
		escreve(msg[i:i+2])
		
main()
