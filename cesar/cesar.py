def main():
    texto = open("texto.txt").read()
    alfabeto = {"A": 0, "B": 0, "C": 0,
                "D": 0, "E": 0, "F": 0,
                "G": 0, "H": 0, "I": 0,
                "J": 0, "K": 0, "L": 0,
                "M": 0, "N": 0, "O": 0,
                "P": 0, "P": 0, "R": 0,
                "S": 0, "T": 0, "U": 0,
                "V": 0, "W": 0, "X": 0,
                "Y": 0, "Z": 0 }

    for alfa in alfabeto:
        count = 0
        for beta in texto:
            if(alfa == beta):
                count += 1
        alfabeto[alfa] = count
    decripta(texto, alfabeto)

def busca(alfabeto):
    moda = "A"
    for l in alfabeto:
        if(alfabeto[l] > alfabeto[moda]):
            moda = l
    return moda

def decripta(texto, alfabeto):
    n = 0
    string = ""
    
    frequencia = "AEOSRINDMUTCLPVGHQBFZJXKWY"
    m = busca(alfabeto)
    
    for i in frequencia:
        f = open("out/out"+str(n)+".txt", "w")
        k = ord(m)-ord(i)
        for j in texto:
            L = ord(j)-k
            if(L < 65):
                L+=26
            string+=(chr(L))
        print("\n"+string)
        f.write(string)

        cmd = input("\nDeseja continuar?(S/n)\n>>> ")
        
        if(cmd == "n" or cmd == "N"):
            return 0
        n+=1
        string = ""
        f.close()
main()
