"""
Daniel: Daniel Silva
Data: 12/10/2018
"""

""" Retorna a letra que mais se repete numa lib """
def moda(lib):
    mod = lib.keys()[0]
    for l in lib:
        if(lib[l] > lib[mod]):
            mod = l
    return mod

""" Retorna um vetor com as modas de cada grupo de indices i """
def vetModa(texto, length):
    res = []
    for i in range(length):
        vet = []
        for j in range(i, len(texto), length):
            vet.append(texto[j])
        res.append(moda(frequence(vet)))
    return res
    
""" Retorna a substring que mais se repete """
def subs(texto):
    ref = ""
    subLib = {}
    i=3 # Tamanhoo da substring
    j=0 # Indice inicial
    # Interador de tamanho
    while(j<len(texto)-i):
        if(i>10):
            break
        ref = texto[j:j+i]
        case = 0
        # Compara substrings
        for k in range(len(texto)-i+1):
            if(ref == texto[k:k+i]):
                case += 1
                subLib.update({ref : case})
        j+=1
        if(j==len(texto)-i):
            print("* Ja encontradas substrings de tamanho: {}".format(i))
            j=0
            i+=1
    return moda(subLib)

""" Calcula de mdc de, no minimo, 90% dos numeross no vetor."""
def mdcBestRiftVector(vet):
    divisor  = vet[0]
    v = []
    fail = 0
    for i in vet:
        if(i < divisor):
            divisor = i
    while(True):
        fail = 0
        for i in range(len(vet)):
            """
            if(vet[i] % divisor):
                divisor-=1
                break
            elif(i == len(vet)-1):
                return divisor
            """
            if(vet[i] % divisor):
                fail += 1
            if(fail/float(len(vet)) > 0.1):
                divisor-=1
                break
            elif(i == len(vet)-1):
                return divisor

""" Calcula a distancia entre duas substrings iguais """
def length(texto, sub):
    l = len(sub)
    result = 0
    p = 0
    vet=[]
    for i in range(len(texto)):
        if((not p) and texto[i:i+l] == sub):
            p = i
        elif(texto[i:i+l] == sub):
            vet.append(result)
            result = 0
        if(p and i >= p+l):
            result += 1
    return mdcBestRiftVector(vet)
        
""" Faz contagem de frequencia do de um vetor """
def frequence(vet):
    alfabeto = {"A": 0, "B": 0, "C": 0,
                "D": 0, "E": 0, "F": 0,
                "G": 0, "H": 0, "I": 0,
                "J": 0, "K": 0, "L": 0,
                "M": 0, "N": 0, "O": 0,
                "P": 0, "P": 0, "R": 0,
                "S": 0, "T": 0, "U": 0,
                "V": 0, "W": 0, "X": 0,
                "Y": 0, "Z": 0 }

    for alfa in alfabeto:
        count = 0
        for beta in vet:
            if(alfa == beta):
                count += 1
        alfabeto[alfa] = count
    return alfabeto

""" Obtem chave a partir de vetor com modas """
def key(vetModas):
    vet = []
    for i in vetModas:
       vet.append(ord(i)-ord('A'))
    return vet 
""" decifra mensagem encritada com a cifrea de Vigenere """  
def decrypet(texto, chave):
    string = ""
    vChar = [i for i in texto]
    
    for k in range(len(chave)):
        for j in range(k, len(texto), len(chave)):
            L = ord(texto[j])-chave[k]
            if(L < 65):
                L+=26
            vChar[j] = chr(L)
    for i in vChar:
        string+=i
    return string

def main():
    print("Lendo arquivo...")
    texto = open("texto.txt").read()
    print("Procurando Substring...")
    sub = subs(texto)
    print("Substring: " + sub)
    print("Calculando tamanho da chave...")
    l = length(texto, sub)
    print("Tamanho: "+str(l))
    print("Calculando chave (1/2)...")
    vModa = vetModa(texto, l)
    print("Calculando chave (2/2)...")
    chave = key(vModa)
    print("Decifrando msg...")
    msg = decrypet(texto, chave)
    print(msg)    
    f = open("out.txt", "w")
    f.write(msg)
    f.close()

main()
